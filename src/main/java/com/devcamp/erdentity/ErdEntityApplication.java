package com.devcamp.erdentity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ErdEntityApplication {

	public static void main(String[] args) {
		SpringApplication.run(ErdEntityApplication.class, args);
	}

}
